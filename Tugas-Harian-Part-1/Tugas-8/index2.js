var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

// Soal 2
console.log("----SOAL 2----");
const timeStart = 10000;
const execute = (time, index) => {
  readBooksPromise(time, books[index])
    .then((timeleft) => {
      if (timeleft !== 0) {
        execute(timeleft, index + 1);
      }
    })
    .catch(() => {});
};
execute(timeStart, 0);
