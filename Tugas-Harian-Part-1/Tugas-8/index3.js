var filterBooksPromise = require("./promise2.js");

// Soal 3
console.log("----SOAL 3----");
var books1 = async () => {
  try {
    let books = await filterBooksPromise(false, 250);
    console.log(books);
  } catch (err) {
    console.log(err.message);
  }
};

var books2 = async () => {
  try {
    let books = await filterBooksPromise(true, 30);
    console.log(books);
  } catch (err) {
    console.log(err.message);
  }
};

filterBooksPromise(true, 40)
  .then((books) => console.log(books))
  .catch((err) => console.log(err.message));
books1();
books2();
