var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "komik", timeSpent: 1000 },
];

// Soal 1
console.log("----SOAL 1----");
const timeStart = 10000;
const execute = (time, index) => {
  readBooks(time, books[index], (timeleft) => {
    if (timeleft !== 0) {
      execute(timeleft, index + 1);
    }
  });
};
execute(timeStart, 0);
