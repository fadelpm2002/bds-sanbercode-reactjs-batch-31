// Soal 1
console.log("\nSoal 1");
var i = 21;
var no = 2;
/* dalam 1 looping */
console.log("Dalam 1 looping");
while (i--) {
  if (i < 10) {
    console.log(no + " - I will become a frontend developer");
    no -= 2;
  } else if (i == 10) {
    console.log("LOOPING KEDUA");
  } else if (i < 20) {
    console.log(no + " - I love coding");
    no += 2;
  } else if (i == 20) {
    console.log("LOOPING PERTAMA");
  }
}
console.log();
/* dalam 2 looping */
console.log("Dalam 2 looping");
i = 0;
while (i <= 20) {
  if (i == 0) {
    console.log("LOOPING PERTAMA");
  } else {
    console.log(i + " - I love coding");
  }
  i += 2;
}
while (i > 0) {
  if (i == 22) {
    console.log("LOOPING KEDUA");
  } else {
    console.log(i + " - I will become a frontend developer");
  }
  i -= 2;
}

// Soal 2
console.log("\nSoal 2");
for (var i = 1; i < 21; i++) {
  if (i % 2 != 0 && i % 3 == 0) {
    console.log(i + " - I Love Coding");
  } else if (i % 2 == 0) {
    console.log(i + " - Berkualitas");
  } else {
    console.log(i + " - Santai");
  }
}

// Soal 3
console.log("\nSoal 3");
var pagar = "#";
i = 1;
while (i < 8) {
  console.log(pagar);
  pagar = pagar + "#";
  i++;
}

// Soal 4
console.log("\nSoal 4");
var kalimat = ["aku", "saya", "sangat", "sangat", "senang", "belajar", "javascript"];
var kalimatOut = kalimat[0];
kalimat.shift();
kalimat.splice(1, 1);
for (var index = 1; index < kalimat.length; index++) {
  kalimatOut = kalimatOut + " " + kalimat[index];
}
console.log(kalimatOut);

// Soal 5
console.log("\nSoal 5");
var sayuran = [];
sayuran.push("Kangkung", "Bayam", "Buncis", "Kubis", "Timun", "Seledri", "Tauge");
sayuran.sort();
for (var index = 0; index < sayuran.length; index++) {
  console.log(index + 1 + ". " + sayuran[index]);
}
