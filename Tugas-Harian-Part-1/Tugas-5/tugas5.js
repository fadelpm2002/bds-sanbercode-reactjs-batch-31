// Soal 1
console.log("-----Soal 1-----");
var panjang = 12;
var lebar = 4;
var tinggi = 8;
function luasPersegiPanjang(p, l) {
  return p * l;
}
function kelilingPersegiPanjang(p, l, t) {
  return 2 * (p + l);
}
function volumeBalok(p, l, t) {
  return p * l * t;
}
var luasPersegiPanjang = luasPersegiPanjang(panjang, lebar);
var kelilingPersegiPanjang = kelilingPersegiPanjang(panjang, lebar);
var volumeBalok = volumeBalok(panjang, lebar, tinggi);
console.log(luasPersegiPanjang);
console.log(kelilingPersegiPanjang);
console.log(volumeBalok);

// Soal 2
console.log("\n-----Soal 2-----");
function introduce(name, age, address, hobby) {
  return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!";
}
var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan belum jadi, dan saya punya hobby yaitu Gaming!"

// Soal 3
console.log("\n-----Soal 3-----");
var arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku", 1992];
var objDaftarPeserta = {
  nama: "John Doe",
  "jenis kelamin": "laki-laki",
  hobi: "baca buku",
  "tahun lahir": 1992,
};
console.log(objDaftarPeserta);

// Soal 4
console.log("\n-----Soal 4-----");
var buah = [
  { nama: "Nanas", warna: "Kuning", "ada bijinya": false, harga: 9000 },
  { nama: "Jeruk", warna: "Orange", "ada bijinya": true, harga: 8000 },
  { nama: "Semangka", warna: "Hijau & Merah", "ada bijinya": true, harga: 10000 },
  { nama: "Pisang", warna: "Kuning", "ada bijinya": false, harga: 5000 },
];
console.log(
  buah.filter(function (item) {
    return item["ada bijinya"] == false;
  })
);

// Soal 5
console.log("\n-----Soal 5-----");
function tambahDataFilm(nama, durasi, genre, tahun) {
  var obj = {
    nama: nama,
    durasi: durasi,
    genre: genre,
    tahun: tahun,
  };
  dataFilm.push(obj);
}
var dataFilm = [];
tambahDataFilm("LOTR", "2 jam", "action", "1999");
tambahDataFilm("avenger", "2 jam", "action", "2019");
tambahDataFilm("spiderman", "2 jam", "action", "2004");
tambahDataFilm("juon", "2 jam", "horror", "2004");
console.log(dataFilm);
