// Soal 1
console.log("Soal 1");
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

console.log(kataPertama + " " + kataKedua.charAt(0).toUpperCase() + kataKedua.slice(1) + " " + kataKetiga.substring(0, kataKetiga.length - 1) + kataKetiga.charAt(kataKetiga.length - 1).toUpperCase() + " " + kataKeempat.toUpperCase());

// Soal 2
console.log("\nSoal 2");
var panjangPersegiPanjang = "8";
var lebarPersegiPanjang = "5";

var alasSegitiga = "6";
var tinggiSegitiga = "7";

var kelilingPersegiPanjang = (parseInt(panjangPersegiPanjang) + parseInt(lebarPersegiPanjang)) * 2;

console.log("Keliling Persegi Panjang = " + kelilingPersegiPanjang);

var luasSegitiga = (parseInt(alasSegitiga) * parseInt(tinggiSegitiga)) / 2;

console.log("Luas Segitiga = " + luasSegitiga);

// Soal 3
console.log("\nSoal 3");
var sentences = "wah javascript itu keren sekali";

var firstWord = sentences.substring(0, 3);
var secondWord = sentences.substring(4, 14);
var thirdWord = sentences.substring(15, 18);
var fourthWord = sentences.substring(19, 24);
var fifthWord = sentences.substring(25, sentences.length);

console.log("Kata Pertama: " + firstWord);
console.log("Kata Kedua: " + secondWord);
console.log("Kata Ketiga: " + thirdWord);
console.log("Kata Keempat: " + fourthWord);
console.log("Kata Kelima: " + fifthWord);

// Soal 4
console.log("\nSoal 4");
var nilaiJohn = 80;
var nilaiDoe = 50;

var nilai = nilaiJohn;
process.stdout.write("Nilai John = ");
if (nilai >= 80) {
  console.log("Nilai A");
} else if (nilai >= 70) {
  console.log("Nilai B");
} else if (nilai >= 60) {
  console.log("Nilai C");
} else if (nilai >= 50) {
  console.log("Nilai D");
} else {
  console.log("Nilai E");
}

var nilai = nilaiDoe;
process.stdout.write("Nilai Doe = ");
if (nilai >= 80) {
  console.log("Nilai A");
} else if (nilai >= 70) {
  console.log("Nilai B");
} else if (nilai >= 60) {
  console.log("Nilai C");
} else if (nilai >= 50) {
  console.log("Nilai D");
} else {
  console.log("Nilai E");
}

// Soal 5
console.log("\nSoal 5");
var tanggal = 25;
var bulan = 5;
var tahun = 2002;

process.stdout.write(tanggal + " ");

switch (bulan) {
  case 1: {
    process.stdout.write("Januari ");
    break;
  }
  case 2: {
    process.stdout.write("Februari ");
    break;
  }
  case 3: {
    process.stdout.write("Maret ");
    break;
  }
  case 4: {
    process.stdout.write("April ");
    break;
  }
  case 5: {
    process.stdout.write("Mei ");
    break;
  }
  case 6: {
    process.stdout.write("Juni ");
    break;
  }
  case 7: {
    process.stdout.write("Juli ");
    break;
  }
  case 8: {
    process.stdout.write("Agustus ");
    break;
  }
  case 9: {
    process.stdout.write("September ");
    break;
  }
  case 10: {
    process.stdout.write("Oktober ");
    break;
  }
  case 11: {
    process.stdout.write("November ");
    break;
  }
  case 12: {
    process.stdout.write("Desember ");
    break;
  }
}
console.log(String(tahun));
