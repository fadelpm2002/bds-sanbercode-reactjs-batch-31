// Soal 1
console.log("Soal 1");
const luasLingkaran = (r) => {
  return Math.round(Math.PI * r * r);
};
const kelilingLingkaran = (r) => {
  return Math.round(2 * Math.PI * r);
};
let luas = luasLingkaran(7);
let keliling = kelilingLingkaran(7);
console.log(luas);
console.log(keliling);

// Soal 2
console.log("\nSoal 2");
const introduce = (...rest) => {
  let [name, age, gender, profession] = rest;
  if (gender == "Laki-Laki") {
    return `Pak ${name} adalah seorang ${profession} yang berusia ${age} tahun`;
  } else {
    return `Bu ${name} adalah seorang ${profession} yang berusia ${age} tahun`;
  }
};
const perkenalan = introduce("John", "30", "Laki-Laki", "penulis");
console.log(perkenalan); // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"

// Soal 3
console.log("\nSoal 3");
const newFunction = function literal(firstName, lastName) {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(firstName + " " + lastName);
    },
  };
};

console.log(newFunction("John", "Doe").firstName);
console.log(newFunction("Richard", "Roe").lastName);
newFunction("William", "Imoh").fullName();

// Soal 4
console.log("\nSoal 4");
let phone = {
  name: "Galaxy Note 20",
  brand: "Samsung",
  year: 2020,
  colors: ["Mystic Bronze", "Mystic White", "Mystic Black"],
};
let { name: phoneName, brand: phoneBrand, year, colors } = phone;
let [colorBronze, , colorBlack] = colors;
console.log(phoneBrand, phoneName, year, colorBlack, colorBronze);

// Soal 5
console.log("\nSoal 5");
let warna = ["biru", "merah", "kuning", "hijau"];
let dataBukuTambahan = {
  penulis: "john doe",
  tahunTerbit: 2020,
};
let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172,
  warnaSampul: ["hitam"],
};
/* Tulis kode jawabannya di sini */
buku.warnaSampul = [...buku.warnaSampul, ...warna];
buku = { ...buku, ...dataBukuTambahan };
console.log(buku);
