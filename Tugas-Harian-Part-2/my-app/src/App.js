import { tab } from "@testing-library/user-event/dist/tab";
import logo from "../src/Tugas-9/img/logo.png";
import Checkbox from "../src/Tugas-9/tugas9.js";
import TimeStamp from "./Tugas-10/tugas10";
import List from "./Tugas-11/tugas11";
import NilaiMahasiswa12 from "./Tugas-12/tugas12";
import NilaiMahasiswa13 from "./Tugas-13/NilaiMahasiswa";
import Routes from "./Tugas-14/Routes";
import "antd/dist/antd.css";

const App = () => <Routes />;

export default App;
