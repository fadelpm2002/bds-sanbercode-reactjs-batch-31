import React from "react";

const Checkbox = (props) => {
  return (
    <div className="checkbox">
      <input type="checkbox" id={props.number} name={props.number} value={props.number} style={{ transform: "scale(1.5)" }} />
      <label htmlFor={props.number} style={{ marginLeft: "10px" }}>
        {" "}
        {props.name}
      </label>
    </div>
  );
};

export default Checkbox;
