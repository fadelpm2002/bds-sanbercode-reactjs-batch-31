import React from "react";
import Checkbox from "./component";
import logo from "./img/logo.png";

const Tugas9 = () => {
  return (
    <div className="container">
      <img src={logo} alt="logo" />
      <section>
        <h2>THINGS TO DO</h2>
        <p className="title">During bootcamp in sanbercode</p>
        <form action="">
          <Checkbox name="Belajar GIT & CLI" number="item1" />
          <Checkbox name="Belajar HTML & CSS" number="item2" />
          <Checkbox name="Belajar Javascript" number="item3" />
          <Checkbox name="Belajar ReactJS Dasar" number="item4" />
          <Checkbox name="Belajar ReactJS Advance" number="item5" />
          <input type="submit" value="SEND" className="button"></input>
        </form>
      </section>
    </div>
  );
};

export default Tugas9;
