import React, { useState } from "react";

const List = () => {
  const [itemObject, setitemObject] = useState([
    { name: "Nanas", totalPrice: "100000", totalWeight: "4 kg", pricePerKG: "25000" },
    { name: "Manggis", totalPrice: "350000", totalWeight: "10 kg", pricePerKG: "35000" },
    { name: "Nangka", totalPrice: "90000", totalWeight: "2 kg", pricePerKG: "45000" },
    { name: "Durian", totalPrice: "400000", totalWeight: "5 kg", pricePerKG: "80000" },
    { name: "Strawberry", totalPrice: "120000", totalWeight: "6 kg", pricePerKG: "20000" },
  ]);
  const [inputName, setInputName] = useState("");
  const [inputPrice, setInputPrice] = useState("");
  const [inputWeight, setInputWeight] = useState("");
  const [currentIndex, setCurrentIndex] = useState(-1);

  const handleDelete = (event) => {
    let index = parseInt(event.target.value);
    let deletedItem = itemObject[index];
    let newData = itemObject.filter((e) => {
      return e !== deletedItem;
    });
    setitemObject(newData);
  };

  const handleChangeName = (event) => {
    setInputName(event.target.value);
  };

  const handleChangePrice = (event) => {
    setInputPrice(event.target.value);
  };

  const handleChangeWeight = (event) => {
    setInputWeight(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    let newData = itemObject;
    let newItem = {
      name: inputName,
      totalPrice: inputPrice,
      totalWeight: parseInt(inputWeight) / 1000 + " kg",
      pricePerKG: parseInt(parseInt(inputPrice * 1000) / parseInt(inputWeight)),
    };
    if (currentIndex === -1) {
      newData = [...itemObject, newItem];
    } else {
      newData[currentIndex] = newItem;
    }
    setitemObject(newData);
    setCurrentIndex(-1);
    setInputName("");
    setInputPrice("");
    setInputWeight("");
  };

  const handleEdit = (event) => {
    let index = parseInt(event.target.value);
    let editValue = itemObject[index];
    setInputName(editValue.name);
    setInputPrice(editValue.totalPrice);
    setInputWeight(parseInt(editValue.totalWeight) * 1000);
    setCurrentIndex(event.target.value);
  };

  return (
    <>
      <h1 className="text-header">Daftar Harga Buah</h1>
      <table>
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Harga total</th>
            <th>Berat total</th>
            <th>Harga per kg</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          {itemObject.map((val, index) => {
            return (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{val.name}</td>
                <td>{val.totalPrice}</td>
                <td>{val.totalWeight}</td>
                <td>{val.pricePerKG}</td>
                <td>
                  <button onClick={handleEdit} value={index}>
                    Edit
                  </button>{" "}
                  <button onClick={handleDelete} value={index}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>

      {/* Form */}
      <h1 className="text-header">Form Daftar Harga Buah</h1>
      <form onSubmit={handleSubmit} className="formBox">
        <label htmlFor="name">Nama:</label>
        <input id="name" type="text" value={inputName} onChange={handleChangeName} required />
        <br />
        <br />
        <label htmlFor="price">Harga Total:</label>
        <input id="price" type="number" value={inputPrice} onChange={handleChangePrice} min={0} required />
        <br />
        <br />
        <label htmlFor="weight">Berat Total(dalam gram):</label>
        <input id="weight" type="number" value={inputWeight} onChange={handleChangeWeight} min={2000} required />
        <br />
        <br />
        <button id="submit">submit</button>
      </form>
    </>
  );
};
export default List;
