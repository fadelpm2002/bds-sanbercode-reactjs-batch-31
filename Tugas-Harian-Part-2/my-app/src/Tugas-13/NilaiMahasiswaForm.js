import React, { useContext } from "react";
import { DataContext } from "./DataProvider";

const NilaiMahasiswaForm = () => {
  const { dataMahasiswa, setDataMahasiswa, inputName, setInputName, inputCourse, setInputCourse, inputScore, setInputScore, currentId, setCurrentId, indexCount, setIndex, functions } = useContext(DataContext);

  const { fetchData, countIndex, functionDelete, functionEdit, functionSubmitAdd, functionSubmitUpdate } = functions;

  const handleChangeName = (event) => {
    setInputName(event.target.value);
  };

  const handleChangeCourse = (event) => {
    setInputCourse(event.target.value);
  };

  const handleChangeScore = (event) => {
    setInputScore(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (currentId === null) {
      functionSubmitAdd();
    } else {
      functionSubmitUpdate();
    }
    setInputName("");
    setInputCourse("");
    setInputScore("");
    setCurrentId(null);
  };

  return (
    <>
      <h1 className="text-header">Form Nilai Mahasiswa</h1>
      <form onSubmit={handleSubmit} className="formBox">
        <label htmlFor="name">
          <b>Nama:</b>{" "}
        </label>
        <input id="name" type="text" value={inputName} onChange={handleChangeName} required />
        <br />
        <br />
        <label htmlFor="course">
          <b>Mata Kuliah:</b>
        </label>
        <input id="course" type="text" value={inputCourse} onChange={handleChangeCourse} required />
        <br />
        <br />
        <label htmlFor="score">
          <b>Nilai:</b>
        </label>
        <input id="score" type="number" value={inputScore} onChange={handleChangeScore} required min={0} max={100} />
        <br />
        <br />
        <button id="submit">Submit</button>
      </form>
    </>
  );
};

export default NilaiMahasiswaForm;
