import React from "react";
import { DataProvider } from "./DataProvider";
import NilaiMahasiswaForm from "./NilaiMahasiswaForm";
import NilaiMahasiswaList from "./NilaiMahasiswaList";

const NilaiMahasiswa13 = () => {
  return (
    <>
      <DataProvider>
        <NilaiMahasiswaList />
        <NilaiMahasiswaForm />
      </DataProvider>
    </>
  );
};

export default NilaiMahasiswa13;
