import React, { useState, createContext } from "react";
import axios from "axios";

export const DataContext = createContext();

export const DataProvider = (props) => {
  const [dataMahasiswa, setDataMahasiswa] = useState([]);
  const [inputName, setInputName] = useState("");
  const [inputCourse, setInputCourse] = useState("");
  const [inputScore, setInputScore] = useState("");
  const [currentId, setCurrentId] = useState(null);
  const [indexCount, setIndex] = useState(0);

  const countIndex = (score) => {
    let index;
    if (score >= 80) {
      index = "A";
    } else if (score >= 70) {
      index = "B";
    } else if (score >= 60) {
      index = "C";
    } else if (score >= 50) {
      index = "D";
    } else {
      index = "E";
    }
    return index;
  };

  const fetchData = async () => {
    const result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`);
    setDataMahasiswa(
      result.data.map((data, index) => {
        return { name: data.name, course: data.course, score: data.score, scoreIndex: countIndex(data.score), id: data.id };
      })
    );
  };

  const fetchById = async (id) => {
    let res = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${id}`);
    setInputName(res.data.name);
    setCurrentId(res.data.id);
    setInputCourse(res.data.course);
    setInputScore(res.data.score);
  };

  const functionDelete = (id) => {
    axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${id}`).then(() => {
      let newData = dataMahasiswa.filter((el) => {
        return el.id !== id;
      });
      setDataMahasiswa(newData);
    });
  };

  const functionEdit = (id) => {
    axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${id}`).then((res) => {
      setInputName(res.data.name);
      setCurrentId(res.data.id);
      setInputCourse(res.data.course);
      setInputScore(res.data.score);
    });
  };

  const functionSubmitAdd = () => {
    axios.post(`http://backendexample.sanbercloud.com/api/student-scores`, { name: inputName, course: inputCourse, score: inputScore }).then((res) => {
      setDataMahasiswa([...dataMahasiswa, { name: inputName, course: inputCourse, score: inputScore, scoreIndex: countIndex(inputScore), id: res.data.id }]);
    });
  };

  const functionSubmitUpdate = () => {
    axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentId}`, { name: inputName, course: inputCourse, score: inputScore }).then(() => {
      let singleMhs = dataMahasiswa.find((el) => el.id === currentId);
      singleMhs.name = inputName;
      singleMhs.course = inputCourse;
      singleMhs.score = inputScore;
      singleMhs.scoreIndex = countIndex(inputScore);
      setDataMahasiswa([...dataMahasiswa]);
    });
  };
  const functions = {
    fetchData,
    countIndex,
    functionDelete,
    functionEdit,
    functionSubmitAdd,
    functionSubmitUpdate,
    fetchById,
  };

  return (
    <DataContext.Provider value={{ dataMahasiswa, setDataMahasiswa, inputName, setInputName, inputCourse, setInputCourse, inputScore, setInputScore, currentId, setCurrentId, indexCount, setIndex, functions }}>
      {props.children}
    </DataContext.Provider>
  );
};
