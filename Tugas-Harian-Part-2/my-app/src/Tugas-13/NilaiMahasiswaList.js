import React, { useEffect, useContext } from "react";
import { DataContext } from "./DataProvider";

const NilaiMahasiswaList = () => {
  const { dataMahasiswa, setDataMahasiswa, inputName, setInputName, inputCourse, setInputCourse, inputScore, setInputScore, currentId, setCurrentId, indexCount, setIndex, functions } = useContext(DataContext);

  const { fetchData, countIndex, functionDelete, functionEdit, functionSubmitAdd, functionSubmitUpdate } = functions;

  useEffect(() => {
    fetchData();
  }, []);

  const handleEdit = (event) => {
    let id = event.target.value;
    functionEdit(id);
  };

  const handleDelete = (event) => {
    let id = parseInt(event.target.value);
    functionDelete(id);
  };

  return (
    <>
      {dataMahasiswa !== null && (
        <>
          <h1 className="text-header">Daftar Nilai Mahasiswa</h1>
          <table className="peserta-lomba">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Mata Kuliah</th>
                <th>Nilai</th>
                <th>Index Nilai</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              {dataMahasiswa.map((item, index) => {
                return (
                  <tr key={item.id}>
                    <td>{index + 1}</td>
                    <td>{item.name}</td>
                    <td>{item.course}</td>
                    <td>{item.score}</td>
                    <td>{item.scoreIndex}</td>
                    <td>
                      <button onClick={handleEdit} value={item.id}>
                        Edit
                      </button>
                      &nbsp;
                      <button onClick={handleDelete} value={item.id}>
                        Delete
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </>
      )}
    </>
  );
};

export default NilaiMahasiswaList;
