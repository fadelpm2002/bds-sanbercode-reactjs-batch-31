import React, { useEffect, useContext, useState } from "react";
import { useHistory, useParams } from "react-router-dom/cjs/react-router-dom.min";
import { DataContext } from "../Tugas-13/DataProvider";
import { message, Button, Table } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";

const NilaiMahasiswaList_v3 = () => {
  const { dataMahasiswa, setDataMahasiswa, inputName, setInputName, inputCourse, setInputCourse, inputScore, setInputScore, currentId, setCurrentId, indexCount, setIndex, functions } = useContext(DataContext);

  const { fetchData, countIndex, functionDelete, functionEdit, functionSubmitAdd, functionSubmitUpdate } = functions;

  let history = useHistory();

  useEffect(() => {
    fetchData();
  }, []);

  const success = () => {
    message.success("Data Terhapus!");
  };

  const handleEdit = (event) => {
    let id = event.currentTarget.value;
    // functionEdit(id);
    history.push(`/tugas15/edit/${id}`);
  };

  const handleDelete = (event) => {
    let id = parseInt(event.currentTarget.value);
    functionDelete(id);
    success();
  };

  const handleToForm = () => {
    history.push("/tugas15/create");
    setInputName("");
    setInputCourse("");
    setInputScore("");
    setCurrentId(null);
  };

  const columns = [
    {
      title: "Nama",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Mata Kuliah",
      dataIndex: "course",
      key: "course",
    },
    {
      title: "Index",
      dataIndex: "score",
      key: "score",
    },
    {
      title: "Index Nilai",
      dataIndex: "scoreIndex",
      key: "scoreIndex",
    },
    {
      title: "Aksi",
      key: "aksi",
      render: (res, index) => (
        <>
          <Button icon={<EditOutlined />} onClick={handleEdit} value={res.id}>
            Edit
          </Button>
          &nbsp; &nbsp;
          <Button icon={<DeleteOutlined />} onClick={handleDelete} value={res.id} />
        </>
      ),
    },
  ];

  return (
    <>
      {/* {dataMahasiswa !== null && ( */}
      <>
        <h1 className="text-header">Daftar Nilai Mahasiswa</h1>
        <Button type="primary" onClick={handleToForm}>
          Buat Data Nilai Mahasiswa Baru
        </Button>{" "}
        <br />
        {/* <table className="peserta-lomba">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Mata Kuliah</th>
                <th>Nilai</th>
                <th>Index Nilai</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              {dataMahasiswa.map((item, index) => {
                return (
                  <>
                    <tr key={item.id}>
                      <td>{index + 1}</td>
                      <td>{item.name}</td>
                      <td>{item.course}</td>
                      <td>{item.score}</td>
                      <td>{item.scoreIndex}</td>
                      <td>
                        <button onClick={handleEdit} value={item.id}>
                          Edit
                        </button>
                        &nbsp;
                        <button onClick={handleDelete} value={item.id}>
                          Delete
                        </button>
                      </td>
                    </tr>
                  </>
                );
              })}
            </tbody>
          </table> */}
        <br />
        <Table columns={columns} dataSource={dataMahasiswa} />
      </>
      {/* )} */}
    </>
  );
};

export default NilaiMahasiswaList_v3;
