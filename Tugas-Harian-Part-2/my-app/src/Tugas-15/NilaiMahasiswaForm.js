import { message, Button } from "antd";
import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { useHistory, useParams } from "react-router-dom/cjs/react-router-dom.min";
import { useEffect } from "react/cjs/react.development";
import { DataContext } from "../Tugas-13/DataProvider";

const NilaiMahasiswaForm_v3 = () => {
  const { dataMahasiswa, setDataMahasiswa, inputName, setInputName, inputCourse, setInputCourse, inputScore, setInputScore, currentId, setCurrentId, indexCount, setIndex, functions } = useContext(DataContext);

  const { fetchData, countIndex, functionDelete, functionEdit, functionSubmitAdd, functionSubmitUpdate, fetchById } = functions;

  let history = useHistory();
  let { Value } = useParams();

  useEffect(() => {
    if (Value !== undefined) {
      fetchById(Value);
    }
  }, []);

  const handleChangeName = (event) => {
    setInputName(event.target.value);
  };

  const handleChangeCourse = (event) => {
    setInputCourse(event.target.value);
  };

  const handleChangeScore = (event) => {
    setInputScore(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (currentId === null) {
      functionSubmitAdd();
      successAdd();
    } else {
      functionSubmitUpdate();
      successEdit();
    }
    setInputName("");
    setInputCourse("");
    setInputScore("");
    setCurrentId(null);
    if (inputScore > -1 && inputScore < 101) {
      history.push("/tugas15");
    }
  };

  const successAdd = () => {
    message.success("Data Berhasil Ditambah!");
  };
  const successEdit = () => {
    message.success("Data Berhasil Diedit!");
  };

  return (
    <>
      <h1 className="text-header">Form Nilai Mahasiswa</h1>
      <form onSubmit={handleSubmit} className="formBox">
        <label htmlFor="name">
          <b>Nama:</b>{" "}
        </label>
        <input id="name" type="text" value={inputName} onChange={handleChangeName} required />
        <br />
        <br />
        <label htmlFor="course">
          <b>Mata Kuliah:</b>
        </label>
        <input id="course" type="text" value={inputCourse} onChange={handleChangeCourse} required />
        <br />
        <br />
        <label htmlFor="score">
          <b>Nilai:</b>
        </label>
        <input id="score" type="number" value={inputScore} onChange={handleChangeScore} required min={0} max={100} />
        <br />
        <br />
        <button id="submit">Submit</button>
        <br />
        <Link to="/tugas15">Kembali Ke Tabel</Link>
      </form>
    </>
  );
};

export default NilaiMahasiswaForm_v3;
