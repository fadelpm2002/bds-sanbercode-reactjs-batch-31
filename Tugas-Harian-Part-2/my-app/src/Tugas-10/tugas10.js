import { useState, useEffect, useRef } from "react";

// function to generate time stamp
const clock = () => {
  var time = "h:m:s r";
  var date = new Date();
  var hours = date.getHours();
  var mins = date.getMinutes();
  if (mins < 10) mins = "0" + mins;
  var secs = date.getSeconds();
  if (secs < 10) secs = "0" + secs;
  var r = "AM";
  if (hours === 12) {
    r = "PM";
  } else if (hours > 12) {
    hours -= 12;
    r = "PM";
  } else if (hours === 0) {
    hours += 12;
  }
  return time.replace("h", hours).replace("m", mins).replace("s", secs).replace("r", r);
};

const TimeStamp = () => {
  const [count, setCount] = useState(100);
  const [bool, setBool] = useState(true);

  const [time, setTime] = useState(clock());
  let timer;

  const func = (count, callback) => {
    timer = setTimeout(() => {
      if (count - 1 === 0) {
        setBool(false);
      } else {
        setTime(clock());
        setCount(count - 1);
        callback(count - 1);
      }
    }, 1000);
  };

  const execute = (count) => {
    func(count, (newCount) => {
      if (newCount !== 0) {
        execute(newCount);
      }
    });
  };

  useEffect(() => {
    execute(count);
    return () => clearTimeout(timer);
  }, []);

  return (
    <>
      {bool ? (
        <div className="time-Stamp">
          <h1>Now At - {time}</h1>
          <h3>Countdown : {count}</h3>
        </div>
      ) : (
        <></>
      )}
    </>
  );
};

export default TimeStamp;
