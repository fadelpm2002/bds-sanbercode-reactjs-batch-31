import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { NavContext } from "./NavProvider";

const Nav = () => {
  const { color, setColor, func } = useContext(NavContext);

  return (
    <div className={`navbar ${color ? "navbar-white" : "navbar-black"}`}>
      <li>
        <Link to="/">Tugas 9</Link>
      </li>
      <li>
        <Link to="/tugas10">Tugas 10</Link>
      </li>
      <li>
        <Link to="/tugas11">Tugas 11</Link>
      </li>
      <li>
        <Link to="/tugas12">Tugas 12</Link>
      </li>
      <li>
        <Link to="/tugas13">Tugas 13</Link>
      </li>
      <li>
        <Link to="/tugas14">Tugas 14</Link>
      </li>
      <li>
        <Link to="/tugas15">Tugas 15</Link>
      </li>
    </div>
  );
};

export default Nav;
