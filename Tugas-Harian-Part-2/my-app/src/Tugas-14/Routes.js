import React from "react";
import TimeStamp from "../Tugas-10/tugas10";
import List from "../Tugas-11/tugas11";
import NilaiMahasiswa12 from "../Tugas-12/tugas12";
import { DataProvider } from "../Tugas-13/DataProvider";
import NilaiMahasiswa13 from "../Tugas-13/NilaiMahasiswa";
import NilaiMahasiswaForm_v3 from "../Tugas-15/NilaiMahasiswaForm";
import NilaiMahasiswaList_v3 from "../Tugas-15/NilaiMahasiswaList";
import Tugas9 from "../Tugas-9/tugas9";
import Nav from "./Nav";
import { NavProvider } from "./NavProvider";
import NilaiMahasiswaForm_v2 from "./NilaiMahasiswaForm";
import NilaiMahasiswaList_v2 from "./NilaiMahasiswaList";
import SwitchTheme from "./SwitchTheme";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

const Routes = () => {
  return (
    <Router>
      <DataProvider>
        <NavProvider>
          <Nav />
          <Switch>
            <Route exact path="/">
              <div className="window">
                <div className="wrap-paper">
                  <Tugas9 />
                </div>
              </div>
            </Route>
            <Route exact path="/tugas10">
              <TimeStamp />
            </Route>
            <Route exact path="/tugas11">
              <div className="window">
                <div className="wrap-paper">
                  <List />
                </div>
              </div>
            </Route>
            <Route exact path="/tugas12">
              <div className="window">
                <div className="wrap-paper">
                  <NilaiMahasiswa12 />
                </div>
              </div>
            </Route>
            <Route exact path="/tugas13">
              <div className="window">
                <div className="wrap-paper">
                  <NilaiMahasiswa13 />
                </div>
              </div>
            </Route>
            <Route exact path="/tugas14">
              <div className="window">
                <div className="wrap-paper">
                  <SwitchTheme />
                  <NilaiMahasiswaList_v2 />
                </div>
              </div>
            </Route>
            <Route exact path="/tugas14/create">
              <div className="window">
                <div className="wrap-paper" style={{ width: "70%" }}>
                  <NilaiMahasiswaForm_v2 />
                </div>
              </div>
            </Route>
            <Route exact path="/tugas15">
              <SwitchTheme />
              <div className="window">
                <div className="wrap-paper" style={{ backgroundColor: "white" }}>
                  <NilaiMahasiswaList_v3 />
                </div>
              </div>
            </Route>
            <Route exact path="/tugas15/create">
              <div className="window">
                <div className="wrap-paper" style={{ width: "70%" }}>
                  <NilaiMahasiswaForm_v3 />
                </div>
              </div>
            </Route>
            <Route exact path="/tugas15/edit/:Value">
              <div className="window">
                <div className="wrap-paper" style={{ width: "70%" }}>
                  <NilaiMahasiswaForm_v3 />
                </div>
              </div>
            </Route>
          </Switch>
        </NavProvider>
      </DataProvider>
    </Router>
  );
};

export default Routes;
