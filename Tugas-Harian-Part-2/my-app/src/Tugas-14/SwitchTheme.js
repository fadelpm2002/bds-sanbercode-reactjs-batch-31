import React, { useContext } from "react";
import { NavContext } from "./NavProvider";
import { Switch } from "antd";
import { CloseOutlined, CheckOutlined } from "@ant-design/icons";

const SwitchTheme = () => {
  const { color, setColor, func } = useContext(NavContext);

  const { handleSetColor } = func;

  return (
    <div style={{ position: "absolute", right: 20, top: 16 }}>
      <Switch onClick={handleSetColor} checkedChildren="Light" unCheckedChildren="Dark" defaultChecked />
    </div>
  );
};

export default SwitchTheme;
