import React, { useState, createContext } from "react";

export const NavContext = createContext();

export const NavProvider = (props) => {
  const [color, setColor] = useState(1);

  const handleSetColor = () => {
    if (color === 1) {
      setColor(0);
    } else {
      setColor(1);
      console.log(color);
    }
  };

  const func = {
    handleSetColor,
  };

  return <NavContext.Provider value={{ color, setColor, func }}>{props.children}</NavContext.Provider>;
};
