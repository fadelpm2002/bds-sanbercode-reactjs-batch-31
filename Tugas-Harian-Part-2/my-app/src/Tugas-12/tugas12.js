import React, { useState, useEffect } from "react";
import axios from "axios";

const NilaiMahasiswa12 = () => {
  const [dataMahasiswa, setDataMahasiswa] = useState([]);
  const [inputName, setInputName] = useState("");
  const [inputCourse, setInputCourse] = useState("");
  const [inputScore, setInputScore] = useState("");
  const [currentId, setCurrentId] = useState(null);
  const countIndex = (score) => {
    let index;
    if (score >= 80) {
      index = "A";
    } else if (score >= 70) {
      index = "B";
    } else if (score >= 60) {
      index = "C";
    } else if (score >= 50) {
      index = "D";
    } else {
      index = "E";
    }
    return index;
  };

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`);
      setDataMahasiswa(
        result.data.map((data) => {
          return { name: data.name, course: data.course, score: data.score, scoreIndex: countIndex(data.score), id: data.id };
        })
      );
    };

    fetchData();
  }, []);

  const handleEdit = (event) => {
    let id = event.target.value;
    axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${id}`).then((res) => {
      setInputName(res.data.name);
      setCurrentId(res.data.id);
      setInputCourse(res.data.course);
      setInputScore(res.data.score);
    });
  };

  const handleDelete = (event) => {
    let id = parseInt(event.target.value);
    axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${id}`).then(() => {
      let newData = dataMahasiswa.filter((el) => {
        return el.id !== id;
      });
      setDataMahasiswa(newData);
    });
  };

  const handleChangeName = (event) => {
    setInputName(event.target.value);
  };

  const handleChangeCourse = (event) => {
    setInputCourse(event.target.value);
  };

  const handleChangeScore = (event) => {
    setInputScore(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (currentId === null) {
      axios.post(`http://backendexample.sanbercloud.com/api/student-scores`, { name: inputName, course: inputCourse, score: inputScore }).then((res) => {
        setDataMahasiswa([...dataMahasiswa, { name: inputName, course: inputCourse, score: inputScore, scoreIndex: countIndex(inputScore), id: res.data.id }]);
      });
    } else {
      axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentId}`, { name: inputName, course: inputCourse, score: inputScore }).then(() => {
        let singleMhs = dataMahasiswa.find((el) => el.id === currentId);
        singleMhs.name = inputName;
        singleMhs.course = inputCourse;
        singleMhs.score = inputScore;
        singleMhs.scoreIndex = countIndex(inputScore);
        setDataMahasiswa([...dataMahasiswa]);
      });
    }
    setInputName("");
    setInputCourse("");
    setInputScore("");
    setCurrentId(null);
  };

  return (
    <>
      {dataMahasiswa !== null && (
        <>
          <h1 className="text-header">Daftar Nilai Mahasiswa</h1>
          <table className="peserta-lomba">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Mata Kuliah</th>
                <th>Nilai</th>
                <th>Index Nilai</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              {dataMahasiswa.map((item, index) => {
                return (
                  <tr key={item.id}>
                    <td>{index + 1}</td>
                    <td>{item.name}</td>
                    <td>{item.course}</td>
                    <td>{item.score}</td>
                    <td>{item.scoreIndex}</td>
                    <td>
                      <button onClick={handleEdit} value={item.id}>
                        Edit
                      </button>
                      &nbsp;
                      <button onClick={handleDelete} value={item.id}>
                        Delete
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </>
      )}
      {/* Form */}
      <h1 className="text-header">Form Nilai Mahasiswa</h1>
      <form onSubmit={handleSubmit} className="formBox">
        <label htmlFor="name">
          <b>Nama:</b>{" "}
        </label>
        <input id="name" type="text" value={inputName} onChange={handleChangeName} required />
        <br />
        <br />
        <label htmlFor="course">
          <b>Mata Kuliah:</b>
        </label>
        <input id="course" type="text" value={inputCourse} onChange={handleChangeCourse} required />
        <br />
        <br />
        <label htmlFor="score">
          <b>Nilai:</b>
        </label>
        <input id="score" type="number" value={inputScore} onChange={handleChangeScore} required min={0} max={100} />
        <br />
        <br />
        <button id="submit">submit</button>
      </form>
    </>
  );
};

export default NilaiMahasiswa12;
