import { Button } from "antd";
import React from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import photo from "../profile.jpg";

const About = () => {
  let history = useHistory();

  const backToHome = (event) => {
    history.push("/");
  };

  return (
    <div className="wrap-paper">
      <div className="about">
        <h1>Data Profile</h1> <br />
        <li style={{ listStyle: "none" }}>
          <img src={photo} style={{ width: "25%" }} />
        </li>
        <li>
          <b>Nama</b>: Fadel Pramaputra Maulana
        </li>
        <li>
          <b>Email</b>: fadelpm2002@gmail.com
        </li>
        <li>
          <b>Gitlab</b>: fadelpm2002
        </li>
        <li>
          <b>Github</b>: fadel2002
        </li>
        <li>
          <b>Instagram</b>: @pmfadel
        </li>
        <br />
      </div>
      <Button type="primary" onClick={backToHome}>
        Back To Home
      </Button>
    </div>
  );
};

export default About;
