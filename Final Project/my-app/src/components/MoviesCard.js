import React, { useContext, useEffect } from "react";
import { MoviesContext } from "../provider/MoviesProvider";

const MoviesCard = () => {
  const { dataMovies, setDataMovies, inputMovies, setInputMovies, functions } = useContext(MoviesContext);

  const { fetchData, functionDelete, functionEdit, functionSubmitAdd, functionSubmitUpdate, fetchById } = functions;

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="wrap-paper" style={{ width: "80%" }} id="movies-card">
      <div style={{ border: "2px solid black", marginBottom: "10px" }}>
        <h1 style={{ textAlign: "center", marginBottom: "0px" }}>
          <b>MOVIES LIST</b>
        </h1>
        <h5 style={{ textAlign: "center" }}>Between 1980 - 2021</h5>
      </div>
      {dataMovies.map((item, index) => {
        return (
          <React.Fragment key={item.id}>
            <div className="card-desc">
              <span>
                <img src={item.image_url} style={{ width: "40%", marginTop: "10px" }} />
              </span>
              <span>
                <h2 style={{ marginBottom: "0px" }}>{item.title}</h2>
                <span style={{ width: "70%" }}>
                  <p style={{ marginBottom: "5px" }}>{item.description}</p>
                  <table>
                    <tbody>
                      <tr>
                        <td className="title">Release Year</td>
                        <td>: {" " + item.year}</td>
                      </tr>
                      <tr>
                        <td className="title">Genre</td>
                        <td>: {item.genre}</td>
                      </tr>
                      <tr>
                        <td className="title">Duration</td>
                        <td>: {item.duration}</td>
                      </tr>
                      <tr>
                        <td className="title">Rating</td>
                        <td>: {item.rating}</td>
                      </tr>
                      <tr>
                        <td className="title">Review</td>
                        <td>: {item.review}</td>
                      </tr>
                    </tbody>
                  </table>
                </span>
              </span>
            </div>
            <hr />
          </React.Fragment>
        );
      })}
    </div>
  );
};

export default MoviesCard;
