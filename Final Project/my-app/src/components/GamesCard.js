import React, { useContext, useEffect } from "react";
import { GamesContext } from "../provider/GamesProvider";

const GamesCard = () => {
  const { dataGames, setDataGames, inputGames, setInputGames, functions } = useContext(GamesContext);

  const { fetchData, functionDelete, functionEdit, functionSubmitAdd, functionSubmitUpdate, fetchById } = functions;

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="wrap-paper" style={{ width: "80%" }} id="games-card">
      <div style={{ border: "2px solid black", marginBottom: "10px" }}>
        <h1 style={{ textAlign: "center", marginBottom: "0px" }}>
          <b>GAMES LIST</b>
        </h1>
        <h5 style={{ textAlign: "center" }}>Between 2000 - 2021</h5>
      </div>
      {dataGames.map((item, index) => {
        return (
          <React.Fragment key={item.id}>
            <div className="card-desc">
              <span>
                <img src={item.image_url} style={{ width: "40%", marginTop: "10px" }} />
              </span>
              <span>
                <h2 style={{ marginBottom: "0px" }}>{item.name}</h2>
                <span style={{ width: "70%" }}>
                  <table>
                    <tbody>
                      <tr>
                        <td className="title">Release Year</td>
                        <td>: {" " + item.release}</td>
                      </tr>
                      <tr>
                        <td className="title">Genre</td>
                        <td>: {item.genre}</td>
                      </tr>
                      <tr>
                        <td className="title">Duration</td>
                        <td>: {item.category}</td>
                      </tr>
                      <tr>
                        <td className="title">Rating</td>
                        <td>: {item.platform}</td>
                      </tr>
                    </tbody>
                  </table>
                </span>
              </span>
            </div>
            <hr />
          </React.Fragment>
        );
      })}
    </div>
  );
};

export default GamesCard;
