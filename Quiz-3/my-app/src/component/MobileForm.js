import { message, Button } from "antd";
import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { useHistory, useParams } from "react-router-dom/cjs/react-router-dom.min";
import { useEffect, useState } from "react/cjs/react.development";
import { DataContext } from "./DataProvider";

const MobileForm = () => {
  const { dataApp, setDataApp, inputData, setInputData, functions, isAndroCheck, setAndroCheck, isIosCheck, setIosCheck } = useContext(DataContext);

  const { fetchData, functionDelete, functionEdit, functionSubmitAdd, functionSubmitUpdate, fetchById, platformFunc } = functions;

  let history = useHistory();
  let { Id } = useParams();


  useEffect(() => {
    if (Id !== undefined) {
      fetchById(Id);
    }
  }, []);


  const handleChangeName = (event) => {
    setInputData({ ...inputData, name: event.target.value });
  };

  const handleChangeCategory = (event) => {
    setInputData({ ...inputData, category: event.target.value });
  };

  const handleChangeDesc = (event) => {
    setInputData({ ...inputData, description: event.target.value });
  };
  const handleChangeReleaseYear = (event) => {
    setInputData({ ...inputData, release_year: event.target.value });
  };
  const handleChangeSize = (event) => {
    setInputData({ ...inputData, size: event.target.value });
  };
  const handleChangeRating = (event) => {
    setInputData({ ...inputData, rating: event.target.value });
  };
  const handleChangePrice = (event) => {
    setInputData({ ...inputData, price: event.target.value });
  };
  const handleChangeImg = (event) => {
    setInputData({ ...inputData, image_url: event.target.value });
  };
  const handleChangeAndroid = () => {
    setAndroCheck(!isAndroCheck);
    setInputData({ ...inputData, is_android_app: !isAndroCheck });
  };
  const handleChangeIos = () => {
    setIosCheck(!isIosCheck);
    setInputData({ ...inputData, is_ios_app: !isIosCheck });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (Id === undefined) {
      functionSubmitAdd();
      successAdd();
    } else {
      functionSubmitUpdate(Id);
      successEdit();
    }
    setInputData([]);
    history.push("/mobile-list");
  };

  const successAdd = () => {
    message.success("Data Berhasil Ditambah!");
  };
  const successEdit = () => {
    message.success("Data Berhasil Diedit!");
  };

  return (
    <div className="wrap-paper">
      <h1 className="text-header" style={{ textAlign: "center" }}>
        Form Nilai Mahasiswa
      </h1>
      <form onSubmit={handleSubmit} className="formBox">
        <label htmlFor="name">
          <b>Nama</b>
          <br />
        </label>
        <input id="name" type="text" value={inputData.name} onChange={handleChangeName} required style={{ width: "100%" }} />

        <label htmlFor="category">
          <b>Category</b>
          <br />
        </label>
        <input id="category" type="text" value={inputData.category} onChange={handleChangeCategory} required style={{ width: "100%" }} />

        <label htmlFor="desc">
          <b>Description</b>
          <br />
        </label>
        <textarea id="desc" type="text" value={inputData.description} onChange={handleChangeDesc} required style={{ width: "100%" }} />

        <label htmlFor="year">
          <b>Release Year</b>
          <br />
        </label>
        <input id="year" type="number" value={inputData.release_year} onChange={handleChangeReleaseYear} required style={{ width: "100%" }} max="2021" min="2007" />

        <label htmlFor="size">
          <b>Size (MB)</b>
          <br />
        </label>
        <input id="size" type="number" value={inputData.size} onChange={handleChangeSize} required style={{ width: "100%" }} />

        <label htmlFor="price">
          <b>Price</b>
          <br />
        </label>
        <input id="price" type="number" value={inputData.price} onChange={handleChangePrice} required style={{ width: "100%" }} />

        <label htmlFor="rating">
          <b>Rating</b>
          <br />
        </label>
        <input id="rating" type="number" value={inputData.rating} onChange={handleChangeRating} required style={{ width: "100%" }} min="0" max="5" />

        <label htmlFor="image_url">
          <b>Iamge Url</b>
          <br />
        </label>
        <input id="image_url" type="text" value={inputData.image_url} onChange={handleChangeImg} required style={{ width: "100%" }} />

        <label>
          <b>Platform</b>
          <br />
        </label>
        <input type="checkbox" id="android" name="android" value={inputData.is_android_app} onChange={handleChangeAndroid} checked={isAndroCheck} />
        <label htmlFor="android"> Android</label>
        <br />
        <input type="checkbox" id="ios" name="ios" value={inputData.is_ios_app} onChange={handleChangeIos} checked={isIosCheck} />
        <label htmlFor="ios"> IOS</label>
        <br />
        <br />
        <button id="submit">Submit</button>
        <br />
        <br />
        <Link to="/mobile-list">Kembali Ke Tabel</Link>
      </form>
    </div>
  );
};

export default MobileForm;
