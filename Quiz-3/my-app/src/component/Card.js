import React, { useContext, useEffect } from "react";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import { DataContext } from "./DataProvider";
import { SearchContext } from "./SearchBar";

const CardC = () => {
  const { dataApp, setDataApp, inputData, setInputData, functions } = useContext(DataContext);

  const { fetchData, functionDelete, functionEdit, functionSubmitAdd, functionSubmitUpdate, fetchById, platformFunc } = functions;

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="wrap-paper">
      {dataApp.map((item, index) => {
        return (
          <div key={item.id}>
            <h3>{item.name}</h3>
            <h5>Release Year : {item.release_year}</h5>
            <img src={item.image_url} style={{ width: "50%", objectFit: "cover" }} />
            <br />
            <span className="description">
              Price: {item.price}
              <br />
              Rating: {item.rating}
              <br />
              Size: {item.size}
              <br />
              Platform: {platformFunc(item.is_android_app, item.is_ios_app)} <br />
              Description: {item.description} <br />
            </span>
            <br />
            <hr />
            <br />
          </div>
        );
      })}
    </div>
  );
};

export default CardC;
