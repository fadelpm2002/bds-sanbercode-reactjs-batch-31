import { HistoryOutlined } from "@ant-design/icons";
import React, { useContext, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom/cjs/react-router-dom.min";
import { DataContext } from "./DataProvider";
import { SearchContext } from "./SearchBar";

const SearchCardC = () => {
  const { dataApp, setDataApp, inputData, setInputData, functions } = useContext(DataContext);

  const { fetchData, functionDelete, functionEdit, functionSubmitAdd, functionSubmitUpdate, fetchById, platformFunc } = functions;

  const { input, setInput, output, setOutput, searchFunctions } = useContext(SearchContext);

  const { funcSearch } = searchFunctions;

  let { valueOfSearch } = useParams();

  useEffect(() => {
    funcSearch(valueOfSearch);
  }, []);

  return (
    <div className="wrap-paper">
      {output.map((item, index) => {
        return (
          <div key={item.id}>
            <h3>{item.name}</h3>
            <h5>Release Year : {item.release_year}</h5>
            <img src={item.image_url} style={{ width: "50%", objectFit: "cover" }} />
            <br />
            <span className="description">
              Price: {item.price}
              <br />
              Rating: {item.rating}
              <br />
              Size: {item.size}
              <br />
              Platform: {platformFunc(item.is_android_app, item.is_ios_app)} <br />
              Description: {item.description} <br />
            </span>
            <br />
            <hr />
            <br />
          </div>
        );
      })}
    </div>
  );
};

export default SearchCardC;
