import React, { useEffect, useContext, useState } from "react";
import { useHistory, useParams } from "react-router-dom/cjs/react-router-dom.min";
import { message, Button, Table } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { DataContext } from "./DataProvider";

const MobileList = () => {
  const { dataApp, setDataApp, inputData, setInputData, functions } = useContext(DataContext);

  const { fetchData, functionDelete, functionEdit, functionSubmitAdd, functionSubmitUpdate, fetchById, platformFunc } = functions;

  let history = useHistory();

  useEffect(() => {
    fetchData();
  }, []);

  const success = () => {
    message.success("Data Terhapus!");
  };

  const handleEdit = (event) => {
    let id = event.currentTarget.value;
    history.push(`/mobile-form/edit/${id}`);
  };

  const handleDelete = (event) => {
    let id = parseInt(event.currentTarget.value);
    functionDelete(id);

    success();
  };

  const handleToForm = () => {
    history.push("/mobile-form");
    setInputData([]);
  };

  const columns = [
    {
      title: "No",
      dataIndex: "no",
      key: "no",
    },
    {
      title: "Nama",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Category",
      dataIndex: "category",
      key: "category",
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
    },
    {
      title: "Release Year",
      dataIndex: "release_year",
      key: "release_year",
    },
    {
      title: "Size",
      dataIndex: "size",
      key: "size",
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "price",
    },
    {
      title: "Rating",
      dataIndex: "rating",
      key: "rating",
    },
    {
      title: "Platform",
      dataIndex: "platform",
      key: "platform",
    },
    {
      title: "Aksi",
      key: "aksi",
      render: (res, index) => (
        <>
          <Button icon={<EditOutlined />} onClick={handleEdit} value={res.id}>
            Edit
          </Button>
          &nbsp; &nbsp;
          <Button icon={<DeleteOutlined />} onClick={handleDelete} value={res.id} />
        </>
      ),
    },
  ];

  return (
    <div className="wrap-paper">
      <>
        <h1 className="text-header" style={{ textAlign: "center" }}>
          Mobile Apps List
        </h1>
        <Button type="primary" onClick={handleToForm}>
          Buat Data Aplikasi Baru
        </Button>{" "}
        <br />
        <Table columns={columns} dataSource={dataApp} />
      </>
    </div>
  );
};

export default MobileList;
