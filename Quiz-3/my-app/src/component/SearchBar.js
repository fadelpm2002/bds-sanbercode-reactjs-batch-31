import React, { useState, createContext, useContext, useEffect } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { DataContext } from "./DataProvider";

export const SearchContext = createContext();

export const SearchProvider = (props) => {
  const { dataApp, setDataApp, data, setdata, functions } = useContext(DataContext);

  const { fetchData, functionDelete, functionEdit, functionSubmitAdd, functionSubmitUpdate, fetchById, platformFunc, toStringPlatform } = functions;

  const [input, setInput] = useState();
  const [output, setOutput] = useState([]);

  const funcSearch = (str) => {
    const filtered = dataApp.filter((data) => {
      return data.name.toLowerCase().includes(str.toLowerCase());
    });

    setOutput(filtered);
  };

  const searchFunctions = {
    funcSearch,
  };

  return <SearchContext.Provider value={{ input, setInput, output, setOutput, searchFunctions }}>{props.children}</SearchContext.Provider>;
};
