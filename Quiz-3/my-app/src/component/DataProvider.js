import React, { useState, createContext } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";

export const DataContext = createContext();

export const DataProvider = (props) => {
  const [dataApp, setDataApp] = useState([]);
  const [inputData, setInputData] = useState([]);
  const [isAndroCheck, setAndroCheck] = useState(true);
  const [isIosCheck, setIosCheck] = useState(true);

  const toStringPlatform = (android, ios) => {
    if (android && ios) {
      return "Android & IOS";
    } else if (android) {
      return "Android";
    } else if (ios) {
      return "IOS";
    } else {
      return "Android & IOS";
    }
  };

  const fetchData = async () => {
    const result = await axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps`);
    setDataApp(
      result.data.map((data, index) => {
        return {
          no: index + 1,
          id: data.id,
          name: data.name,
          release_year: data.release_year,
          category: data.category,
          description: data.description,
          size: data.size,
          price: data.price,
          rating: data.rating,
          image_url: data.image_url,
          is_android_app: data.is_android_app,
          is_ios_app: data.is_ios_app,
          platform: toStringPlatform(data.is_android_app, data.is_ios_app),
        };
      })
    );
  };

  const fetchById = async (Id) => {
    const result = await axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps/${Id}`);
    let data = result.data;
    setInputData({
      id: Id,
      name: data.name,
      release_year: data.release_year,
      category: data.category,
      description: data.description,
      size: data.size,
      price: data.price,
      rating: data.rating,
      image_url: data.image_url,
      is_android_app: data.is_android_app,
      is_ios_app: data.is_ios_app,
    });

    if ((data.is_ios_app == null && data.is_android_app == null) || (data.is_ios_app == false && data.is_android_app == false)) {
      data.is_android_app = true;
      data.is_ios_app = true;
    }
    setIosCheck(data.is_ios_app);
    setAndroCheck(data.is_android_app);
  };

  let history = useHistory();

  const functionDelete = (id) => {
    axios.delete(`http://backendexample.sanbercloud.com/api/mobile-apps/${id}`).then(() => {
      let newData = dataApp.filter((el) => {
        return el.id !== id;
      });
      newData.map((map, index) => {
        newData[index].no = index + 1;
      });
      setDataApp(newData);
    });

    history.push("/mobile-list");
  };

  const functionEdit = (id) => {
    axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps/${id}`).then((res) => {
      let data = res.data;
      setInputData({
        id: data.id,
        name: data.name,
        release_year: data.release_year,
        category: data.category,
        description: data.description,
        size: data.size,
        price: data.price,
        rating: data.rating,
        image_url: data.image_url,
        is_android_app: data.is_android_app,
        is_ios_app: data.is_ios_app,
      });
    });
  };

  const functionSubmitAdd = () => {
    axios
      .post(`http://backendexample.sanbercloud.com/api/mobile-apps`, {
        name: inputData.name,
        release_year: inputData.release_year,
        category: inputData.category,
        description: inputData.description,
        size: inputData.size,
        price: inputData.price,
        rating: inputData.rating,
        image_url: inputData.image_url,
        is_android_app: inputData.is_android_app,
        is_ios_app: inputData.is_ios_app,
      })
      .then((res) => {
        setDataApp([
          ...dataApp,
          {
            no: dataApp.at(-1).no + 1,
            id: res.data.id,
            name: inputData.name,
            release_year: inputData.release_year,
            category: inputData.category,
            description: inputData.description,
            size: inputData.size,
            price: inputData.price,
            rating: inputData.rating,
            image_url: inputData.image_url,
            is_android_app: inputData.is_android_app,
            is_ios_app: inputData.is_ios_app,
            platform: toStringPlatform(inputData.is_android_app, inputData.is_ios_app),
          },
        ]);
      });
  };

  const functionSubmitUpdate = (id) => {
    axios
      .put(`http://backendexample.sanbercloud.com/api/mobile-apps/${id}`, {
        name: inputData.name,
        release_year: inputData.release_year,
        category: inputData.category,
        description: inputData.description,
        size: inputData.size,
        price: inputData.price,
        rating: inputData.rating,
        image_url: inputData.image_url,
        is_android_app: inputData.is_android_app,
        is_ios_app: inputData.is_ios_app,
      })
      .then(() => {
        let singleFilm = dataApp.find((el) => el.id == id);

        singleFilm.id = id;
        singleFilm.name = inputData.name;
        singleFilm.release_year = inputData.release_year;
        singleFilm.category = inputData.category;
        singleFilm.description = inputData.description;
        singleFilm.size = inputData.size;
        singleFilm.price = inputData.price;
        singleFilm.rating = inputData.rating;
        singleFilm.image_url = inputData.image_url;
        singleFilm.is_android_app = inputData.is_android_app;
        singleFilm.is_ios_app = inputData.is_ios_app;
        singleFilm.platform = toStringPlatform(inputData.is_android_app, inputData.is_ios_app);
        setDataApp([...dataApp]);
      });
  };

  const platformFunc = (android, ios) => {
    if (android && ios) {
      return <>Android & IOS</>;
    } else if (android) {
      return <>Android</>;
    } else {
      return <>IOS</>;
    }
  };

  const functions = {
    fetchData,
    functionDelete,
    functionEdit,
    functionSubmitAdd,
    functionSubmitUpdate,
    fetchById,
    platformFunc,
    toStringPlatform,
  };

  return <DataContext.Provider value={{ dataApp, setDataApp, inputData, setInputData, functions, isAndroCheck, setAndroCheck, isIosCheck, setIosCheck }}>{props.children}</DataContext.Provider>;
};
