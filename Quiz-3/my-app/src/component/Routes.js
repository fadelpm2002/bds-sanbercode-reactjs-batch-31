import { BrowserRouter as Router, Switch, Route } from "react-router-dom/cjs/react-router-dom.min";
import About from "./About";
import CardC from "./Card";
import { DataProvider } from "./DataProvider";
import FooterC from "./Footer";
import MobileForm from "./MobileForm";
import MobileList from "./MobileList";
import Nav from "./Nav";
import { SearchProvider } from "./SearchBar";
import SearchCardC from "./SearchCard";

const Routes = () => {
  return (
    <Router>
      <DataProvider>
        <SearchProvider>
          <Nav />
          <Switch>
            <Route exact path="/">
              <CardC />
              <FooterC />
            </Route>
            <Route exact path="/mobile-list">
              <MobileList />
            </Route>
            <Route exact path="/about">
              <About />
            </Route>
            <Route exact path="/mobile-form">
              <MobileForm />
            </Route>
            <Route exact path="/mobile-form/edit/:Id">
              <MobileForm />
            </Route>
            <Route exact path="/search/:valueOfSearch">
              <SearchCardC />
            </Route>
          </Switch>
        </SearchProvider>
      </DataProvider>
    </Router>
  );
};

export default Routes;
