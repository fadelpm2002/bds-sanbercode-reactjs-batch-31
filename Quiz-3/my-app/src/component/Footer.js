import React from "react";

const FooterC = () => {
  return (
    <div className="footer">
      <h5>&copy; Quiz 3 ReactJS Sanbercode</h5>
    </div>
  );
};

export default FooterC;
