import { Button } from "antd";
import React, { useContext } from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { DataContext } from "./DataProvider";

const About = () => {
  const { dataApp, setDataApp, inputData, setInputData, functions } = useContext(DataContext);

  const { fetchData, functionDelete, functionEdit, functionSubmitAdd, functionSubmitUpdate, fetchById, platformFunc } = functions;

  let history = useHistory();

  const handleEdit = (event) => {
    let id = event.currentTarget.value;
    functionEdit(id);
    history.push("/");
  };

  return (
    <div className="wrap-paper">
      <div className="about">
        <h1>Data Peserta Sanbercode Bootcamp Reactjs</h1> <br />
        <li>
          <b>Nama</b>: Fadel Pramaputra Maulana
        </li>
        <li>
          <b>Email</b>: fadelpm2002@gmail.com
        </li>
        <li>
          <b>Sistem Operasi yang digunakan</b>: Windows
        </li>
        <li>
          <b>Akun Gitlab</b>: fadelpm2002
        </li>
        <li>
          <b>Akun Telegram</b>: Fadel
        </li>
        <br />
      </div>
      <Button type="primary" onClick={handleEdit}>
        Back To Home
      </Button>
    </div>
  );
};

export default About;
