import React, { useContext } from "react";
import { Link } from "react-router-dom";
import logo from "../assets/img/logo.png";
import { Input } from "antd";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { SearchContext } from "./SearchBar";
import { useState } from "react/cjs/react.development";

const Nav = () => {
  const { input, setInput, output, setOutput, searchFunctions } = useContext(SearchContext);

  const [inputed, setInputed] = useState(false);

  let history = useHistory();
  const onSearch = (value) => {
    console.log("tes");
    setInputed(true);
    history.push(`/search/${value}`);
  };

  const setInputFalse = () => {
    setInputed(false);
  };

  const { Search } = Input;
  return (
    <div className="navbar">
      <span>
        <li style={{ width: "100px" }}>
          <Link to="/" style={{ display: "flex" }} onClick={setInputFalse}>
            <img src={logo} style={{ width: "100px" }}></img>
          </Link>
        </li>
        <li>
          <Link to="/" onClick={setInputFalse}>
            Home
          </Link>
        </li>
        <li>
          <Link to="/mobile-list" onClick={setInputFalse}>
            Mobile App List
          </Link>
        </li>
        <li>
          <Link to="/about" onClick={setInputFalse}>
            About
          </Link>
        </li>
      </span>

      {/* <li>
        <Link to="/mobile-form"></Link>
      </li>
      <li>
        <Link to="/mobile-form/edit/:id"></Link>
      </li>
      <li>
        <Link to="/search/:valueOfSearch"></Link>
      </li> */}
      {inputed ? (
        <Link to="/" onClick={setInputFalse}>
          Back
        </Link>
      ) : (
        <li>
          <Search placeholder="input search text" allowClear enterButton="Cari" onSearch={onSearch} />
        </li>
      )}
    </div>
  );
};

export default Nav;
