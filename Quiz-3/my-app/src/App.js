import "./App.css";
import Routes from "./component/Routes";

const App = () => {
  return <Routes />;
};

export default App;
